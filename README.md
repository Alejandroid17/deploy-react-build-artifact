# How to use


## Create build and upload to bitbucket-download

1. In [Personal settings > App passwords](https://bitbucket.org/account/settings/app-passwords/) create and **App password**.
    1. With permisions:
        - Repositories: Write permission.
        - Pipelines: Write permission.
    2. Save the **password.**

2. In **Project > Repository settings > Repository variables** create two variables:
    - **BITBUCKET_USERNAME**: Bitbucket username of the repository owner.
    - **BITBUCKET_APP_PASSWORD**: Password saved in **step 1.2.**

3. On **commit** in main, the pipeline will be executed and the artefacts will be saved in **Project > Downloads**

## Download file from bitbucket-download

Using the API:


```
curl -s -L https://${BITBUCKET_USERNAME}:${BITBUCKET_APP_PASSWORD}@api.bitbucket.org/2.0/repositories/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/downloads/<filename> --output <filename> 
```

# References

- [Bitbucket: API downloads filename.](https://developer.atlassian.com/bitbucket/api/2/reference/resource/repositories/%7Bworkspace%7D/%7Brepo_slug%7D/downloads/%7Bfilename%7D#get)
- [Bitbucket: Variables and secrets.](https://support.atlassian.com/bitbucket-cloud/docs/variables-and-secrets/)
- [Bitbucket: Deploy build artifacts to Bitbucket Downloads.](https://support.atlassian.com/bitbucket-cloud/docs/deploy-build-artifacts-to-bitbucket-downloads/)



